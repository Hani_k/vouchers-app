# Voucher App:

## First, clone the repository to your local machine:
```cmd
git clone https://Hani_k@bitbucket.org/Hani_k/vouchers-app.git
```

## Setup the local configurations::
### Create virtual enviroment:
```cmd
cd vouchers-app
py -m venv ./venv
```

### Activate virtual enviroment:
```cmd
venv\Scripts\activate
```

### Install requierments:
```cmd
pip install -r requierments.txt
```

## Create dataBase:
```cmd
py manage.py migrate
```

## Run the development server:
```cmd
py manage.py runserver
```

> The project will be available at 127.0.0.1:8000
