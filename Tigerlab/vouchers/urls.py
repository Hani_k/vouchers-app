from django.urls import path
from . import views

urlpatterns = [
    path('', views.VoucherView.as_view(), name='get_discount'),
    path('discount/<voucher_code>/', views.display_discount, name='display_discount'),
]
