from django.conf import settings
from django.core.validators import MinValueValidator
from django.db import models
from django.utils import timezone


class Voucher (models.Model):
    MALYSIAN_RINGGET = 'MR'
    PERCENTAGEG = '%'
    discount_type_choices = [
        (MALYSIAN_RINGGET, 'Malysian Ringget'),
        (PERCENTAGEG, 'Percentage')
    ]
    code = models.CharField(max_length=20)
    voucher_type = models.CharField(
        max_length=2,
        choices=discount_type_choices,
        default=PERCENTAGEG
    )
    value = models.DecimalField(
        max_digits=6,
        decimal_places=2,
        default=10,
        validators=[MinValueValidator(0,"Negative values are invalid")]
    )
    times_used = models.IntegerField(
        default=0,
        validators=[MinValueValidator(0,"Negative values are invalid")]
     )
    expired = models.BooleanField(default=False)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_date = models.DateTimeField(default=timezone.now)
    modified_date = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.code

    def check_validation(self):
        """ Returns expired if the code used mroe than 3 times """
        if self.times_used >= 3:
            self.expired = True
        else:
            self.expired = False

    def save(self, *args, **kwargs):
        self.modified_date = timezone.now()
        update_times_used = kwargs.pop('update_times_used', False)
        if update_times_used:
            self.times_used += 1
        self.check_validation()
        super(Voucher, self).save(*args, **kwargs)
