from django import forms

from .models import Voucher


class VoucherForm(forms.Form):
    """
    Handel the voucher template form
    """
    code = forms.CharField(max_length=100)

    def clean_code(self):
        """Vaildate the code field in the input."""
        code = self.cleaned_data.get('code', None)
        if not code:
            raise forms.ValidationError('This code dose not exist.')
        try:
            voucher = Voucher.objects.get(code=code)
        except Voucher.DoesNotExist:
            raise forms.ValidationError('This code dose not exist.')
        if voucher.expired == True:
            raise forms.ValidationError(
                'This code has been used more than 3 times.')
        else:
            voucher.save(update_times_used=True)
        return code
