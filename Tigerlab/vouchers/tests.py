from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse

from .forms import VoucherForm
from .models import Voucher


class VoucherTestCase(TestCase):

    def setUp(self):
        """
        Create fake User, Vouchers
        """
        user = User.objects.create_user(
            username="user1",
            email="user1@utest.com",
            password="test1234"
        )
        Voucher.objects.create(code="A1B3", created_by=user,
                               value=10, voucher_type='MR')
        Voucher.objects.create(code="A2B4", value='10.00',
                               created_by=user, voucher_type='%')

    def test_contains_form(self):
        """
        Chech if the from is an instance of VoucherForm
         """
        url = reverse('get_discount')
        response = self.client.get(url)
        form = response.context.get('form')
        self.assertIsInstance(form, VoucherForm)

    def test_vouchers_validation_count(self):
        """
        Check vouchers validation
        """
        voucher_1 = Voucher.objects.get(code="A1B3")
        voucher_1.times_used += 4
        voucher_1.save()
        voucher_1.check_validation()
        voucher_2 = Voucher.objects.get(code="A2B4")
        voucher_2.times_used += 1
        voucher_2.save()
        voucher_2.check_validation()
        # test that voucher have been expired after 3 usage
        self.assertEqual(voucher_1.expired, True)
        # test voucher times_used after 1 usage
        self.assertEqual(voucher_2.times_used, 1)

    def test_different_voucher_types(self):
        """
         Check voucher different types 
         """

        url = reverse('display_discount', kwargs={"voucher_code": "A1B3"})
        data = {
            "code": "A1B3"
        }
        response = self.client.post(url, data)
        url_2 = reverse('display_discount', kwargs={"voucher_code": "A2B4"})
        data_2 = {
            "code": "A2B4"
        }
        response_2 = self.client.post(url_2, data_2)
        self.assertContains(response, "MR", 1)
        self.assertContains(response_2, "%", 1)
