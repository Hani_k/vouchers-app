from django import forms
from django.contrib import admin

from .models import Voucher


class VoucherAdminForm(forms.ModelForm):
    """
     Handel the form for the Voucher model in the site-admin
     """

    def clean_value(self):
        """ Validate the value field to be less than 100"""
        voucher_type = self.cleaned_data['voucher_type']
        value = self.cleaned_data['value']
        if value and (value > 100) and voucher_type == '%':
            raise forms.ValidationError(
                "Percentage value must be in range 0 to 100")
        return value


class VoucherAdmin(admin.ModelAdmin):
    """
     Provied admin option for the Voucher model 
    """
    form = VoucherAdminForm
    list_display = ('code', 'voucher_type', 'value',
                    'times_used', 'created_by', 'expired')
    list_display_links = ('code',)


admin.site.register(Voucher, VoucherAdmin)
