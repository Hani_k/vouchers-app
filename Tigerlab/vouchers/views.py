from django.shortcuts import redirect, render
from django.views.generic.edit import FormView

from .forms import VoucherForm
from .models import Voucher


class VoucherView(FormView):
    """
    A view to display VoucherForm and render the 'voucher' template.
    """
    template_name = 'voucher.html'
    form_class = VoucherForm

    def form_valid(self, form):
        """If the form is valid, redirect to the display_discount URL."""
        code = form.cleaned_data['code']
        return redirect('display_discount', voucher_code=code)


def display_discount(request, voucher_code):
    """
    Render the 'display_discount' template 

    Parameters:
        voucher_code(str): used to get the Voucher Model instance.

    Returns:
        'display_discount' template.

    """
    voucher = Voucher.objects.get(code=voucher_code)
    return render(request, 'display_discount.html', {
        'voucher': voucher,
        'discount': f'{voucher.value} {voucher.voucher_type}'
    })
