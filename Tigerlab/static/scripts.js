const formEle = document.getElementById("form");


formEle.addEventListener('submit', (e) => {
    const inputEle = event.currentTarget.querySelector('#id_code')
    if (inputEle.value.trim() == '') {
        event.preventDefault();
        inputEle.value = 'this field is required'
        inputEle.style.color = 'red';
        inputEle.style.borderColor = 'red';
    }
    setTimeout(() => {
        inputEle.value = ''
        inputEle.style.color = '#fff';
        inputEle.style.borderColor = '#eee';
    }, 2000)
});